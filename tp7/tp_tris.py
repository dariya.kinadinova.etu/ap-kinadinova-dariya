from random import shuffle
import timeit
import matplotlib.pyplot as plt
from analyse_tris import tri_select
from math import sqrt
from analyse_tris import analyser_tri
from typing import Callable
from compare import compare
from ap_decorators import count
from tris import *


# Préliminaires
def liste_alea(n: int) -> list[int]:
    """construit une liste de longueur n contenant les entiers 0 à n-1 mélangés

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    l = list(range(n))
    shuffle(l)
    return l

# Évaluation expérimentale de la complexité en temps
compare = count(compare)

def analyser_tri(tri: Callable[[list[T], Callable[[T, T], int]], NoneType],
                 nbre_essais: int,
                 taille: int) -> float:
    """
    renvoie: le nombre moyen de comparaisons effectuées par l'algo tri
         pour trier des listes de taille t, la moyenne étant calculée
         sur n listes aléatoires.
    précondition: n > 0, t >= 0, la fonc
    """
    res = 0
    for i in range(nbre_essais):
        compare.counter = 0
        l = [k for k in range(taille)]
        shuffle(l)
        tri(l, compare)
        res += compare.counter
    return res / nbre_essais

Nmax = 100
number = 5000

best_case = []
average_case = []
worst_case = []

for t in range(1, Nmax + 1):
    # Best Case: Sorted List
    sorted_list = list(range(t))
    best_time = timeit.timeit(stmt='tri_insert(liste)', setup='from __main__ import tri_insert, liste', globals={'liste': sorted_list}, number=number)
    best_case.append(best_time)

    # Average Case: Random List
    random_list = random.sample(range(t), t)
    average_time = timeit.timeit(stmt='tri_insert(liste)', setup='from __main__ import tri_insert, liste; import random', globals={'liste': random_list}, number=number)
    shuffle_time = timeit.timeit(stmt='random.shuffle(liste)', setup='import random; liste = list(range(t))', globals={'t': t, 'random': random}, number=number)
    average_case.append(average_time - shuffle_time)

    # Worst Case: Reverse Sorted List
    reverse_sorted_list = list(range(t - 1, -1, -1))
    worst_time = timeit.timeit(stmt='tri_insert(liste[::-1])', setup='from __main__ import tri_insert, liste', globals={'liste': reverse_sorted_list}, number=number)
    reverse_time = timeit.timeit(stmt='liste.reverse()', setup='liste = list(range(t))', globals={'t': t}, number=number)
    worst_case.append(worst_time - reverse_time)

# Plotting
plt.plot(range(1, Nmax + 1), best_case, label='Best Case')
plt.plot(range(1, Nmax + 1), average_case, label='Average Case')
plt.plot(range(1, Nmax + 1), worst_case, label='Worst Case')
plt.xlabel('Length of List')
plt.ylabel('Time (s)')
plt.title('Insertion Sort Time Complexity')
plt.legend()
plt.grid(True)
plt.show()

