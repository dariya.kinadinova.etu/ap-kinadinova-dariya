#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:mod:`image récursive` module

:author: Dariya Kinadinova

:date: 03/2024


"""
from PIL import Image, ImageDraw
from color_manipulation import *
from block import *
import sys

# Algorithme récursif
def rec_algorithm(image: Image, order:int) -> 'Block':
    """Cet algorithme permet de créer des blocs imbriqués les uns dans les autres \
    à partir d'une image.

    Précondition : order >= 0
    Exemple(s) :
    $$$ 

    """
    average_color = avg_col(list_col(image, (0, 0), (image.size[0]-1, image.size[1]-1)))
    rec_blocks = []
    if order != 0:
        # Divise image en quatre sous-blocs
        blocks = decouper(image)
        for block in blocks:
            # executer l'algorithme sur chaque sous-bloc en diminuant l'ordre de 1
            rec_blocks.append(rec_algorithm(block, order-1))
        if rec_blocks[0].are_4_close(rec_blocks[1], rec_blocks[2], rec_blocks[3]):
            # si les quatre blocs sont uniforms et proches, alors creer un bloc uniforme de couleur \
            # la couleur moyenne des quatre blocs
            res = Block(True, rec_blocks[0].get_average_color(rec_blocks[1], rec_blocks[2], \
                                                              rec_blocks[3]))
        else:
            # sinon, il cee et renvoie un bloc avec les quatre blocs
            res = Block(False, rec_blocks[0], rec_blocks[1], rec_blocks[2], rec_blocks[3])
    else:
        # il cree et renvoie un bloc de la couleur moyenne de l'image
        res = Block(True, average_color)
    return res

def main():
    """permet d'utiliser le terminal

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    if len(sys.argv) != 3:
        print("usage: image_rec.py image.png order")
    else:
        im_path = sys.argv[1]
        order = int(sys.argv[2])
        im = Image.open(im_path)
        block = rec_algorithm(im, order)
        res_im = block.block_to_image(im.size)
        res_im.show()

if __name__ == "__main__":
    main()

# Le terminal doit être ouvert dans le dossier du projet
# Par exemple:
# $ python3 codes\image_rec.py images\calbuth.png 5 
    