#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:mod:`block` module

:author: Dariya Kinadinova

:date: 03/2024


"""
from PIL import Image, ImageDraw
from color_manipulation import *

class Block:
    def __init__(self, *args):
        """Initialiser un nouveau bloc.

        Précondition : 
        Exemple(s) :
        $$$ bloc1 = Block(True, (240, 189, 255))
        $$$ bloc2 = Block(True, (255, 198, 240))
        $$$ bloc3 = Block(True, (247, 185, 245))
        $$$ bloc4 = Block(True, (235, 179, 245))
        $$$ bloc1.are_4_close(bloc2, bloc3, bloc4)
        True
        $$$ bloc1.get_average_color(bloc2, bloc3, bloc4)
        (244, 187, 246)
        $$$ big_block = Block(False, bloc1, bloc2, bloc3, bloc4)
        $$$ big_block.upper_left_block.get_average_color(big_block.upper_right_block, big_block.lower_left_block, big_block.lower_right_block)
        (244, 187, 246)
        
        """
        assert len(args) >= 1 and len(args) <= 5 , "1, 2, 3, 4 ou 5 arguments"
        self.is_uniform = args[0]
        if self.is_uniform:
            assert len(args) == 2, "Le bloc uniforme doit avoir deux arguments"
            assert type(args[1]) == tuple and len(args[1]) == 3, "La couleur doit être un tuple de 3 éléments"
            self.color = args[1]
        else:
            assert len(args) == 5, "Le bloc non uniforme doit avoir quatre arguments representant les sous-blocs"
            self.upper_left_block = args[1]
            self.upper_right_block = args[2]
            self.lower_left_block = args[3]
            self.lower_right_block = args[4]
            
    def are_4_close(self, block2: 'Block', block3: 'Block', block4: 'Block') -> bool:
        """Vérifiez si les couleurs de quatre blocs différents sont proches.

        Précondition : doit fournir 3 blocs en plus du premier bloc
        Exemple(s) :
        $$$ 

        """
        if self.is_uniform and block2.is_uniform and block3.is_uniform and block4.is_uniform:
            return is_col_close(self.color, block2.color) and \
                   is_col_close(self.color, block3.color) and \
                   is_col_close(self.color, block4.color)
        else:
            return False
        
    def get_average_color(self, block2: 'Block', block3: 'Block', block4: 'Block') -> tuple:
        """Calcule la couleur moyenne de quatre blocs.

        Précondition : doit fournir 3 blocs en plus du premier bloc
        Exemple(s) :
        $$$ 

        """
        r_avg = (self.color[0] + block2.color[0] + block3.color[0] + block4.color[0]) // 4
        g_avg = (self.color[1] + block2.color[1] + block3.color[1] + block4.color[1]) // 4
        b_avg = (self.color[2] + block2.color[2] + block3.color[2] + block4.color[2]) // 4
        return (r_avg, g_avg, b_avg)
    
    def block_to_image(self, size: tuple) -> Image:
        """Transformer un bloc en image.

        Précondition : 
        Exemple(s) :
        $$$
        
        """
        if self.is_uniform:
        # Si le bloc est uniforme, une nouvelle image est cree
            im = Image.new('RGB', size, self.color)
        else:
        # Calculer la taille
            sub_block_size = (size[0] // 2, size[1] // 2)
        # transformer chaque sous-bloc en image
            upper_left_im = self.upper_left_block.block_to_image(sub_block_size)
            upper_right_im = self.upper_right_block.block_to_image(sub_block_size)
            lower_left_im = self.lower_left_block.block_to_image(sub_block_size)
            lower_right_im = self.lower_right_block.block_to_image(sub_block_size)
        # Creer une nouvelle image et coller les sous-blocs
            im = Image.new('RGB', size)
            im.paste(upper_left_im, (0, 0))
            im.paste(upper_right_im, (sub_block_size[0], 0))
            im.paste(lower_left_im, (0, sub_block_size[1]))
            im.paste(lower_right_im, (sub_block_size[0], sub_block_size[1]))
        return im


