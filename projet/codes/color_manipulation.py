#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:mod:`manipulation des couleurs` module

:author: Dariya Kinadinova

:date: 03/2024


"""
from PIL import Image, ImageDraw

# Découper l'image
def decouper(im: Image) -> list[Image]:
    """Divise l'image en quatre blocs.

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    im_rgb = im.convert('RGB')
    size = im_rgb.size
    w = size[0]
    h = size[1]
    crop1 = (0, 0, (w//2), (h//2))
    block_1 = im.crop(crop1)
    crop2 = ((w//2), 0, w, (h//2))
    block_2 = im.crop(crop2)
    crop3 = (0, (h//2), (w//2), h)
    block_3 = im.crop(crop3)
    crop4 = ((h//2), (w//2), h, w)
    block_4 = im.crop(crop4)
    l  = [block_1, block_2, block_3, block_4]
    return l

# Manipulation des couleurs
def avg_col(l:list) -> tuple():
    """Renvoie la couleur moyenne d'une liste de couleurs

    Précondition : 
    Exemple(s) :
    $$$ avg_col([(236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111)])
    (236, 210, 111)

    """
    r = 0
    g = 0
    b = 0
    for col in l:
        r += col[0]
        g += col[1]
        b += col[2]
    avg_r = r // len(l)
    avg_g = g // len(l)
    avg_b = b // len(l)
    return (avg_r, avg_g, avg_b)

def list_col(im: Image, left_top: tuple(), right_bottom: tuple()) -> list[int]:
    """Renvoie une liste de tuples de couleurs pour chaque pixel de la région spécifiée de l'image.

    Précondition : 
    Exemple(s) :
    $$$ list_col(Image.open('calbuth.png'), (0,0), (1, 1))
    [(236, 210, 111), (236, 210, 111), (236, 210, 111), (236, 210, 111)]
    
    """
    x_min, y_min = left_top
    x_max, y_max = right_bottom
    return [im.getpixel((x, y)) for x in range(x_max+1) for y in range(y_max+1)]

def is_col_close(color1: tuple(), color2: tuple()) -> bool:
    """renvoie True si la distance entre deux couleurs est inférieure ou égale à 30,
    renvoie False si la distance est supérieure à 30.

    Précondition : 
    Exemple(s) : 
    $$$ is_col_close((230, 210, 210), (236, 210, 211))
    True
    $$$ is_col_close((66, 135, 245), (103, 179, 82))
    False

    """
    return (abs(color1[0] - color2[0]) <= 30) and (abs(color1[1] - color2[1]) <= 30) \
           and (abs(color1[2] - color2[2]) <= 30)


    