﻿### Images récursives
### Dariya KINADINOVA 

#### **06/03/24 :**
- J'ai lu les documents et j'ai crée un dossier "projet" et un fichier "ReadMe.md" pour documenter le processus.

#### **13/03/24 :**
- J'ai commencé à travailler sur la classe Block (j'ai commencé par _ _ init _ _ et j'ai ajouté les paramètres pour is_uniform, color et coordinates) et j'ai créée deux fonctions pour la manipulation des couleurs (pour la moyenne des couleurs et leur distance).

#### **20/03/24 :**
- Travail sur Block.py et Decouper_Image.py

#### **27/03/24 :**
- J'ai refait les fonctions decouper, ajouté les fonctions list_col et is_col_close. J'ai créée les fonctions average_color, divide_blocks pour la classe Block en utilisant les fonctions précédemment créées. Travail sur Image_Recursive.

#### **28/03/24 :**
- J'ai décidé de refaire la classe Block car l'algorithme récursif ne fonctionnait pas comme prévu. J'ai décidé de mettre la plupart des fonctions nécessaires à l'algorithme à l'intérieur de la classe.
- Dans block.py pour les arguments dans __init__ j'ai utilisé *args avec l'aide de sa documentation sur le site. J'ai utilisé l'argument is_uniform comme argument principal, ce qui permettra de faire la différence entre un bloc et un block contenant 4 blocs. 
- Au lieu de vérifier si le bloc est uniforme, j'ai créée une fonction qui vérifie si 4 blocs sont uniformes pour l'utiliser dans l'algorithme.

#### **29/03/24 :**
- J'ai travaillé sur image_rec, au lieu qu'il retourne une image, j'ai décidé qu'il retournerait un bloc que je transformerai en image dans le code plus tard.

#### **30/03/24 :**
- Pour average_col, je l'ai modifié pour qu'il ne prenne pas un bloc mais quatre et qu'il envoie leur couleur moyenne, ce qui est plus utile pour image_rec.
- J'ai terminé image_rec.

#### **31/03/24**:
- Pour transformer le bloc en image, j'ai créée une fonction block_to_image à l'intérieur de la classe. Fini la fonction main() qui permettra d'utiliser la fonction rec_algorithm dans le terminal.
- j'ai renommé certaines fonctions, ajouté des exemples, complété la docstring et ajouté des commentaires sur certaines fonctions.

