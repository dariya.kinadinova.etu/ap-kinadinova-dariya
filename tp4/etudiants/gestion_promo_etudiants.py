# Noms : KINADINOVA
# Prenoms : Dariya
# Groupe : MI15
# Date : 07/02/24

from date import Date
from etudiant import Etudiant

# 1
def pour_tous(seq_bool: bool) -> bool:
    """
    Renvoie True ssi `seq_bool` ne contient pas False

    Exemples:

    $$$ pour_tous([])
    True
    $$$ pour_tous((True, True, True))
    True
    $$$ pour_tous((True, False, True))
    False
    """
    if False in seq_bool:
        res = False
    else:
        res = True
    return res

# 2
def il_existe(seq_bool: bool) -> bool:
    """

    Renvoie True si seq_bool contient au moins une valeur True, False sinon

    Exemples:

    $$$ il_existe([])
    False
    $$$ il_existe((False, True, False))
    True
    $$$ il_existe((False, False))
    False
    """
    if True in seq_bool:
        res = True
    else:
        res = False
    return res

# Q1
def charge_fichier_etudiants(fname: str) -> list[Etudiant]:
    """
    Renvoie la liste des étudiants présents dans le fichier dont
    le nom est donné en paramètre.

    précondition: le fichier est du bon format.
    """
    res = []
    with open(fname, 'r') as fin:
        fin.readline()
        ligne = fin.readline()
        while ligne != '':
            nip, nom, prenom, naissance, formation, groupe = ligne.strip().split(';')
            y, m, d = naissance.split('-')
            date_naiss = Date(int(d.lstrip('0')), int(m.lstrip('0')), int(y))
            res.append(Etudiant(nip, nom, prenom, date_naiss, formation, groupe))
            ligne = fin.readline()
    return res

# Q2
L_ETUDIANTS = charge_fichier_etudiants("etudiants.csv")
COURTE_LISTE = L_ETUDIANTS[:10]
VC = L_ETUDIANTS[:2]

# Q3
def est_liste_d_etudiants(x) -> bool:
    """
    Renvoie True si ``x`` est une liste de fiches d'étudiant, False dans le cas contraire.

    Précondition: aucune

    Exemples:

    $$$ est_liste_d_etudiants(COURTE_LISTE)
    True
    $$$ est_liste_d_etudiants("Timoleon")
    False
    $$$ est_liste_d_etudiants([('12345678', 'Calbuth', 'Raymond', 'Danse', '12') ])
    False
    """
    res = isinstance(x, list)
    if res == True:
        for i in x:
            res = isinstance(i, Etudiant)
    return res

# Q4
NBRE_ETUDIANTS = len(L_ETUDIANTS)

# Q5
NIP = "42321214"

def meme_nip(n) -> str:
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ meme_nip("49734124")
    'Eugène Cordier'
    $$$ meme_nip(L_ETUDIANTS[55].nip)
    'Gabrielle Dumas'

    """
    for i in L_ETUDIANTS:
        if n == i.nip:
            return str(i)
    return "Il n'y a personne. / There is no such student."
        
# >>> meme_nip(NIP)
# "Il n'y a personne. / There is no such student."

# Q6
def age_20(l:list[Etudiant]) -> list:
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    res = []
    for i in l:
        while i in l:
            if (i.naissance).annee <= 2004:
                if (i.naissance).mois <= 2:
                    if (i.naissance).jour <= 2:
                        res.append(i)
    return res
        
# Q7
# def ensemble_des_formations(liste: list[Etudiant]) -> set[str]:
#     """
#     Renvoie un ensemble de chaînes de caractères donnant les formations
#     présentes dans les fiches d'étudiants
# 
#     Précondition: liste ne contient que des fiches d'étudiants
# 
#     Exemples:
# 
#     $$$ ensemble_des_formations(COURTE_LISTE)
#     {'MI', 'PEIP', 'MIASHS', 'LICAM'}
#     $$$ ensemble_des_formations(COURTE_LISTE[0:2])
#     {'MI', 'MIASHS'}
#     """
#     res = set()
#     for i in liste:
        
        
