# Noms : KINADINOVA
# Prenoms : Dariya
# Groupe : MI15
# Date : 07/02/24

# 1
def pour_tous(seq_bool: bool) -> bool:
    """
    Renvoie True ssi `seq_bool` ne contient pas False

    Exemples:

    $$$ pour_tous([])
    True
    $$$ pour_tous((True, True, True))
    True
    $$$ pour_tous((True, False, True))
    False
    """
    if False in seq_bool:
        res = False
    else:
        res = True
    return res

# 2
def il_existe(seq_bool: bool) -> bool:
    """

    Renvoie True si seq_bool contient au moins une valeur True, False sinon

    Exemples:

    $$$ il_existe([])
    False
    $$$ il_existe((False, True, False))
    True
    $$$ il_existe((False, False))
    False
    """
    if True in seq_bool:
        res = True
    else:
        res = False
    return res

# Q1


