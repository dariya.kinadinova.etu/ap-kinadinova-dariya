# KINADINOVA Dariya
# MI15
# 17/01/24
# Anagrammes

# Method split
# 1.
s = "la méthode split est parfois bien utile"

# >>> s.split(' ')
# ['la', 'méthode', 'split', 'est', 'parfois', 'bien', 'utile']

# >>> s.split('e')
# ['la méthod', ' split ', 'st parfois bi', 'n util', '']

# >>> s.split('é')
# ['la m', 'thode split est parfois bien utile']

# >>> s.split()
# ['la', 'méthode', 'split', 'est', 'parfois', 'bien', 'utile']

# >>> s.split('')
# Traceback (most recent call last):
#   File "<stdin>", line 1, in <module>
# ValueError: empty separator

# >>> s.split('split')
# ['la méthode ', ' est parfois bien utile']

# 2.
# function split divides or splits the string at the separator which we give to the fucntion 

# 3.
# no, the function split does not modify the string



# Method join
# 1.
# l = s.split()

# >>> "".join(l)
# 'laméthodesplitestparfoisbienutile'

# >>> " ".join(l)
# 'la méthode split est parfois bien utile'

# >>> ";".join(l)
# 'la;méthode;split;est;parfois;bien;utile'

# >>> " tralala ".join(l)
# 'la tralala méthode tralala split tralala est tralala parfois tralala bien tralala utile'

# >>> print ("\n".join(l))
# la
# méthode
# split
# est
# parfois
# bien
# utile

# >>> "".join(s)
# 'la méthode split est parfois bien utile'

# >>> "!".join(s)
# 'l!a! !m!é!t!h!o!d!e! !s!p!l!i!t! !e!s!t! !p!a!r!f!o!i!s! !b!i!e!n! !u!t!i!l!e'

# >>> "".join()
# Traceback (most recent call last):
#   File "<stdin>", line 1, in <module>
# TypeError: str.join() takes exactly one argument (0 given)

# >>> "".join([])
# ''

# >>> "".join([1,2])
# Traceback (most recent call last):
#   File "<stdin>", line 1, in <module>
# TypeError: sequence item 0: expected str instance, int found

# 2.
# function join joins all elements together in one string by putting another element between them which we give to the function, or if it's a single string it puts them between each character

# 3.
# no, it does not modify

# 4.
def join(chaine: str, l: list[str]) -> str:
    """renvoie une chaîne de caractères construite en concaténant toutes les chaînes de l en intercalant s sans utiliser la méthode homonyme

    Précondition : 
    Exemple(s) :
    $$$ join('.', ['raymond', 'calbuth', 'ronchin', 'fr'])
    'raymond.calbuth.ronchin.fr'

    """
    res = ''
    for i in range(0, len(l)-1):
        res = res + l[i] + chaine
    res = res + l[len(l)-1]
    return res



# Méthode sort
# 1.
# l = list('timoleon')

# >>> l
# ['t', 'i', 'm', 'o', 'l', 'e', 'o', 'n']

# s = "Je n'ai jamais joué de flûte."
# l = list(s)

# >>> l
# ['J', 'e', ' ', 'n', "'", 'a', 'i', ' ', 'j', 'a', 'm', 'a', 'i', 's', ' ', 'j', 'o', 'u', 'é', ' ', 'd', 'e', ' ', 'f', 'l', 'û', 't', 'e', '.']

# >>> l.sort()
# >>> l
# [' ', ' ', ' ', ' ', ' ', "'", '.', 'J', 'a', 'a', 'a', 'd', 'e', 'e', 'e', 'f', 'i', 'i', 'j', 'j', 'l', 'm', 'n', 'o', 's', 't', 'u', 'é', 'û']

# it sorts the string starting with punctuation marks followed by capital letters and then smaller lowercase letters in alphabetical order, finishing with letters with accents

# 2.
# l = ['a', 1]

# >>> l.sort()
# Traceback (most recent call last):
#   File "<stdin>", line 1, in <module>
# TypeError: '<' not supported between instances of 'int' and 'str'

# the function sort does not accept both types str and int at the same time or just the type str in general



# Une fonction sort pour les chaînes
# 1.
def sort(s: str) -> str:
    """
    Renvoie une chaîne de caractères contenant les caractères de `s` triés dans l'ordre croissant.

    Précondition:

    Exemples:

    $$$ sort('timoleon')
    'eilmnoot'
    """
    res = ''
    l = list(s)
    (l).sort()
    for elt in l:
        res = res + elt
    return res



# Anagrammes
# 1)
def sont_anagrammes1(s1: str, s2: str) -> bool:
    """
    Renvoie True si s1 et s2 sont anagrammatiques, False sinon.

    Précondition:

    Exemples:

    $$$ sont_anagrammes1('orange', 'organe')
    True
    $$$ sont_anagrammes1('orange','Organe')
    False
    $$$ sont_anagrammes1('orange', 'orangé')
    False
    """
    l1, l2 = list(s1), list(s2)
    (l1).sort()
    (l2).sort()
    return l1 == l2


    
# 2)
def sont_anagrammes2(s1: str, s2: str) -> bool:
    """
    Renvoie True si s1 et s2 sont anagrammatiques, False sinon.

    Précondition:

    Exemples:

    $$$ sont_anagrammes2('orange', 'organe')
    True
    $$$ sont_anagrammes2('orange','Organe')
    False
    $$$ sont_anagrammes2('orange', 'orangé')
    False
    """
    dict1 = dict()
    dict2 = dict()
    for elt in s1:
        if elt in dict1:
            dict1[elt] = dict1[elt] + 1
        else:
            dict1[elt] = 1
    for elt in s2:
        if elt in dict2:
            dict2[elt] = dict2[elt] + 1
        else:
            dict2[elt] = 1    
    d1, d2 = list(dict1), list(dict2)
    (d1).sort(), (d2).sort()
    return d1 == d2

# 3)
def sont_anagrammes3(s1: str, s2: str) -> bool:
    """
    Renvoie True si s1 et s2 sont anagrammatiques, False sinon.

    Précondition:

    Exemples:

    $$$ sont_anagrammes3('orange', 'organe')
    True
    $$$ sont_anagrammes3('orange','Organe')
    False
    $$$ sont_anagrammes3('orange', 'orangé')
    False
    """
    for le in s1:
        if s1.count(le) != s2.count(le):
            return False
    return True
        


# Casse et accentuation
# 1.
EQUIV_NON_ACCENTUE = {"á": "a", "à": "a", "ä": "a", "é": "e", "è": 'e', 'ê': 'e', 'ë': 'e', 'í': 'i', 'î': 'i', 'ï': 'i', 'ó': 'o', 'ô': 'o', 'ö': 'o', 'ú': 'u', 'û': 'u', 'ü': 'u', 'ç': 'c', 'ù': 'u'}
    
# 2.
def bas_casse_sans_accent(mot: str) -> str:
    """
    renvoie une chaîne de caractères identiques à celle passée en paramètre sauf pour les lettres majuscules et les lettres accentuées qui sont converties en leur équivalent minuscule non accentué

    Précondition : 
    Exemple(s) :
    $$$ bas_casse_sans_accent('Orangé')
    'orange'

    """
    res = ''
    for i in mot:
        if i != i.lower():
            i = i.lower()
        if (i in EQUIV_NON_ACCENTUE) == True:
            i = EQUIV_NON_ACCENTUE[i]
            res = res + i
        else:            
            res = res + i
    return res
    
# 3.
def sont_anagrammes_der(mot1: str, mot2: str) -> bool:
    """
    prédicat qui ne différencie pas les lettres selon leur casse ou leur accentuation.

    Précondition : 
    Exemple(s) :
    $$$ sont_anagrammes_der('Orangé', 'organE')
    True
    """
    return sont_anagrammes3(bas_casse_sans_accent(mot1), bas_casse_sans_accent(mot2))

# Le lexique
from lexique import LEXIQUE

def anagrammes(mot: str) -> list[str]:
    """
    Renvoie la liste des mots figurant dans le LEXIQUE qui sont des anagrammes de mot.

    Précondition:

    Exemples:

    $$$ anagrammes('orange')
    ['onagre', 'orange', 'orangé', 'organe', 'rongea']
    $$$ anagrammes('info')
    ['foin']
    $$$ anagrammes('Calbuth')
    []
    """
    res = []
    for i in range(len(LEXIQUE)):
        if sont_anagrammes3(LEXIQUE[i], mot) == True:
            res.append(LEXIQUE[i])
    return res



