# Noms : KINADINOVA
# Prenoms : Dariya
# Groupe : MI15
# Date : 31/01/24

import turtle

# turtle.setpos(-500, 0)

turtle.speed(0)

#Courbe de Von Koch
# turtle.setpos(-350, 0)
def von_koch(l: float, n: int) -> None:
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    if n == 0:
        turtle.forward(l)
    else:  
        for i in [60, -120, 60, 0]:
            von_koch(l, n-1)
            turtle.left(i)
            
# one side only:
#         turtle.forward(l//3)
#         turtle.left(60)
#         turtle.forward(l//3)
#         turtle.left(-120)
#         turtle.forward(l//3)
#         turtle.left(60)
#         turtle.forward(l//3)
#          
#         turtle.left(60)
#         turtle.forward(l//3)
#         turtle.left(60)
#         turtle.forward(l//3)
#         turtle.left(-120)
#         turtle.forward(l//3)
#         turtle.left(60)
#         turtle.forward(l//3)
#         
#         turtle.left(-120)
#         turtle.forward(l//3)
#         turtle.left(60)
#         turtle.forward(l//3)
#         turtle.left(-120)
#         turtle.forward(l//3)
#         turtle.left(60)
#         turtle.forward(l//3)
#         
#         turtle.left(60)
#         turtle.forward(l//3)
#         turtle.left(60)
#         turtle.forward(l//3)
#         turtle.left(-120)
#         turtle.forward(l//3)
#         turtle.left(60)
#         turtle.forward(l//3)
            
# print(von_koch(100, 4))

#Le flocon de Von Koch
# turtle.setpos(-350, 170)

def flocon(l: float, n: int):
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    if n == 0:
        turtle.forward(l)
    else:
        for i in range(6):
            von_koch(l, n+1)
            turtle.left(-120)
            
            
        
# print(flocon(2, 4))
