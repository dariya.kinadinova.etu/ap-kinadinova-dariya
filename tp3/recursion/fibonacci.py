# Noms : KINADINOVA
# Prenoms : Dariya
# Groupe : MI15
# Date : 31/01/24

from ap_decorators import count

#1.
#Si n=10, les nombres fibonacci sont 0,1,1,2,3,5,8,13,21,34,55.

#2.
@count
def fibo(n: int) -> int:
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ fibo(10)
    55
    $$$ fibo(4)
    3

    """
    if n >= 2:
        res = fibo(n-1) + fibo(n-2)
    elif n == 1:
        res = 1
    else:
        res = 0
    return res

#3.
# >>> fibo(40)
# 102334155

#4.
# >>> fibo.counter = 0
# >>> fibo(4)
# 3
# >>> fibo.counter
# 9

#5.
# >>> fibo.counter = 0
# >>> fibo(1)
# 1
# >>> fibo.counter
# 1
# >>> fibo.counter = 0
# >>> fibo(10)
# 55
# >>> fibo.counter
# 177
# >>> fibo.counter = 0
# >>> fibo(40)
# 102334155
# >>> fibo.counter
# 331160281

