# Noms : KINADINOVA
# Prenoms : Dariya
# Groupe : MI15
# Date : 31/01/24

from ap_decorators import trace

# 1.
@trace
def somme(a: int, b: int) -> int:
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ somme(2, 3)
    5
    $$$ somme(6, 1)
    7
    
    """
    if a == 0:
        res = b
    else:
        res = somme(a-1, b+1)
    return res

# >>> somme(5, 8)
#  -> somme((5, 8), {})
# ... -> somme((4, 9), {})
# ...... -> somme((3, 10), {})
# ......... -> somme((2, 11), {})
# ............ -> somme((1, 12), {})
# ............... -> somme((0, 13), {})
# ............... <- 13
# ............ <- 13
# ......... <- 13
# ...... <- 13
# ... <- 13
#  <- 13
# 13

@trace
def binomial(n: int, p: int) -> int:
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : n >= p and p >= 0
    Exemple(s) :
    $$$ binomial(5, 2)
    10

    """
    if p == 0 or p == n:
        res = 1
    else:
        res = binomial(n-1, p) + binomial(n-1, p-1)
    return res

# >>> binomial(3, 2)
#  -> binomial((3, 2), {})
# ... -> binomial((2, 2), {})
# ... <- 1
# ... -> binomial((2, 1), {})
# ...... -> binomial((1, 1), {})
# ...... <- 1
# ...... -> binomial((1, 0), {})
# ...... <- 1
# ... <- 2
#  <- 3
# 3

@trace    
def is_palindromic(mot: str) -> bool:
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ is_palindromic('next')
    False
    $$$ is_palindromic('level')
    True
    $$$ is_palindromic('abcba')
    True

    """
    if len(mot) <= 1:
        res = True
    elif mot[0] == mot[-1]:
        res = is_palindromic(mot[1:-1])
    else:
        res = False
    return res

# >>> is_palindromic('elle')
#  -> is_palindromic(('elle',), {})
# ... -> is_palindromic(('ll',), {})
# ...... -> is_palindromic(('',), {})
# ...... <- True
# ... <- True
#  <- True
# True

